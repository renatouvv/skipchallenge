package com.skip.challenge.service;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.skip.challenge.model.Address;
import com.skip.challenge.model.Restaurant;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SkipChallengeServiceImplTest {
	
	@Autowired
	SkipChallengeService service;

	@Test
	public void getBasePlaceForNearSearches() {
		Address address = service.getBasePlaceForNearSearches();
		assertTrue(address != null);
	}
	
	@Test
	public void findNearPlaces() {
		Address address = service.getBasePlaceForNearSearches();
		List<Restaurant> restaurants = service.findNearPlaces(address);
		assertTrue(restaurants != null);
	}

}