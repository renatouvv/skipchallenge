package com.skip.challenge.controller;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import com.skip.challenge.model.Address;
import com.skip.challenge.model.Restaurant;
import com.skip.challenge.service.SkipChallengeService;

@Controller
@Path("/skip")
public class SkipChallengeController {
	
	@Autowired
	SkipChallengeService service;
	
	@SuppressWarnings("static-access")
	@GET
	@Path("/restaurants")
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<List<Restaurant>> retrieveAllUsers() {
		Address address = service.getBasePlaceForNearSearches();
		List<Restaurant> restaurants = service.findNearPlaces(address);
		if (restaurants == null || restaurants.isEmpty()) {
			return new ResponseEntity<List<Restaurant>> (HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Restaurant>>(HttpStatus.OK).ok(restaurants);
	}

}