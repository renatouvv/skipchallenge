package com.skip.challenge.configuration;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import com.skip.challenge.controller.SkipChallengeController;

@Configuration
public class JerseyConfig extends ResourceConfig {

	public JerseyConfig() {
    	registerEndpoints();
    }
    
    private void registerEndpoints() {
    	register(SkipChallengeController.class);
    }
	
}