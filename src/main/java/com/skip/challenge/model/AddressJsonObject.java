package com.skip.challenge.model;

import java.util.List;

public class AddressJsonObject {
	
	public class AddressComponent {
		public String long_name;
		
		public String short_name;
		
		public List<String> types;
		
		public String getLong_name() {
			return long_name;
		}
		public void setLong_name(String long_name) {
			this.long_name = long_name;
		}
		public String getShort_name() {
			return short_name;
		}
		public void setShort_name(String short_name) {
			this.short_name = short_name;
		}
		public List<String> getTypes() {
			return types;
		}
		public void setTypes(List<String> types) {
			this.types = types;
		}
	}

	public class Location {
		public double lat;
		
		public double lng;

		public double getLat() {
			return lat;
		}

		public void setLat(double lat) {
			this.lat = lat;
		}

		public double getLng() {
			return lng;
		}

		public void setLng(double lng) {
			this.lng = lng;
		}
		
	}

	public class Northeast {
		public double lat;
		
		public double lng;

		public double getLat() {
			return lat;
		}

		public void setLat(double lat) {
			this.lat = lat;
		}

		public double getLng() {
			return lng;
		}

		public void setLng(double lng) {
			this.lng = lng;
		}
		
	}

	public class Southwest {
		public double lat;
		
		public double lng;

		public double getLat() {
			return lat;
		}

		public void setLat(double lat) {
			this.lat = lat;
		}

		public double getLng() {
			return lng;
		}

		public void setLng(double lng) {
			this.lng = lng;
		}
		
	}

	public class Viewport {
		public Northeast northeast;
		
		public Southwest southwest;

		public Northeast getNortheast() {
			return northeast;
		}

		public void setNortheast(Northeast northeast) {
			this.northeast = northeast;
		}

		public Southwest getSouthwest() {
			return southwest;
		}

		public void setSouthwest(Southwest southwest) {
			this.southwest = southwest;
		}
		
	}

	public class Geometry {
		public Location location;
		
		public String location_type;
		
		public Viewport viewport;

		public Location getLocation() {
			return location;
		}

		public void setLocation(Location location) {
			this.location = location;
		}

		public String getLocation_type() {
			return location_type;
		}

		public void setLocation_type(String location_type) {
			this.location_type = location_type;
		}

		public Viewport getViewport() {
			return viewport;
		}

		public void setViewport(Viewport viewport) {
			this.viewport = viewport;
		}
		
	}

	public class Result {
		public List<AddressComponent> address_components;
		
		public String formatted_address;
		
		public Geometry geometry;
		
		public String place_id;
		
		public List<String> types;

		public List<AddressComponent> getAddress_components() {
			return address_components;
		}

		public void setAddress_components(List<AddressComponent> address_components) {
			this.address_components = address_components;
		}

		public String getFormatted_address() {
			return formatted_address;
		}

		public void setFormatted_address(String formatted_address) {
			this.formatted_address = formatted_address;
		}

		public Geometry getGeometry() {
			return geometry;
		}

		public void setGeometry(Geometry geometry) {
			this.geometry = geometry;
		}

		public String getPlace_id() {
			return place_id;
		}

		public void setPlace_id(String place_id) {
			this.place_id = place_id;
		}

		public List<String> getTypes() {
			return types;
		}

		public void setTypes(List<String> types) {
			this.types = types;
		}
		
	}

	public class ResultJsonObject {
		
		public List<Result> results;
		
		public String status;

		public List<Result> getResults() {
			return results;
		}

		public void setResults(List<Result> results) {
			this.results = results;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}
		
	}
}