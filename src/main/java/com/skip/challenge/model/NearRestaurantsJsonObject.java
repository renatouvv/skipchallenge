package com.skip.challenge.model;

import java.util.ArrayList;

public class NearRestaurantsJsonObject {

	public class Location {
		
		private double lat;

		public double getLat() {
			return this.lat;
		}

		public void setLat(double lat) {
			this.lat = lat;
		}

		private double lng;

		public double getLng() {
			return this.lng;
		}

		public void setLng(double lng) {
			this.lng = lng;
		}

		@Override
		public String toString() {
			return "Location [lat=" + lat + ", lng=" + lng + "]";
		}
		
	}

	public class Northeast {
		private double lat;

		public double getLat() {
			return this.lat;
		}

		public void setLat(double lat) {
			this.lat = lat;
		}

		private double lng;

		public double getLng() {
			return this.lng;
		}

		public void setLng(double lng) {
			this.lng = lng;
		}

		@Override
		public String toString() {
			return "Northeast [lat=" + lat + ", lng=" + lng + "]";
		}
		
	}

	public class Southwest {
		private double lat;

		public double getLat() {
			return this.lat;
		}

		public void setLat(double lat) {
			this.lat = lat;
		}

		private double lng;

		public double getLng() {
			return this.lng;
		}

		public void setLng(double lng) {
			this.lng = lng;
		}

		@Override
		public String toString() {
			return "Southwest [lat=" + lat + ", lng=" + lng + "]";
		}
		
	}

	public class Viewport {
		private Northeast northeast;

		public Northeast getNortheast() {
			return this.northeast;
		}

		public void setNortheast(Northeast northeast) {
			this.northeast = northeast;
		}

		private Southwest southwest;

		public Southwest getSouthwest() {
			return this.southwest;
		}

		public void setSouthwest(Southwest southwest) {
			this.southwest = southwest;
		}

		@Override
		public String toString() {
			return "Viewport [northeast=" + northeast + ", southwest=" + southwest + "]";
		}
		
	}

	public class Geometry {
		private Location location;

		public Location getLocation() {
			return this.location;
		}

		public void setLocation(Location location) {
			this.location = location;
		}

		private Viewport viewport;

		public Viewport getViewport() {
			return this.viewport;
		}

		public void setViewport(Viewport viewport) {
			this.viewport = viewport;
		}

		@Override
		public String toString() {
			return "Geometry [location=" + location + ", viewport=" + viewport + "]";
		}
		
	}

	public class OpeningHours {
		private boolean open_now;

		public boolean getOpenNow() {
			return this.open_now;
		}

		public void setOpenNow(boolean open_now) {
			this.open_now = open_now;
		}

		private ArrayList<Object> weekday_text;

		public ArrayList<Object> getWeekdayText() {
			return this.weekday_text;
		}

		public void setWeekdayText(ArrayList<Object> weekday_text) {
			this.weekday_text = weekday_text;
		}

		@Override
		public String toString() {
			return "OpeningHours [open_now=" + open_now + ", weekday_text=" + weekday_text + "]";
		}
		
	}

	public class Photo {
		private int height;

		public int getHeight() {
			return this.height;
		}

		public void setHeight(int height) {
			this.height = height;
		}

		private ArrayList<String> html_attributions;

		public ArrayList<String> getHtmlAttributions() {
			return this.html_attributions;
		}

		public void setHtmlAttributions(ArrayList<String> html_attributions) {
			this.html_attributions = html_attributions;
		}

		private String photo_reference;

		public String getPhotoReference() {
			return this.photo_reference;
		}

		public void setPhotoReference(String photo_reference) {
			this.photo_reference = photo_reference;
		}

		private int width;

		public int getWidth() {
			return this.width;
		}

		public void setWidth(int width) {
			this.width = width;
		}

		@Override
		public String toString() {
			return "Photo [height=" + height + ", html_attributions=" + html_attributions + ", photo_reference="
					+ photo_reference + ", width=" + width + "]";
		}
		
	}

	public class Result {
		private Geometry geometry;

		public Geometry getGeometry() {
			return this.geometry;
		}

		public void setGeometry(Geometry geometry) {
			this.geometry = geometry;
		}

		private String icon;

		public String getIcon() {
			return this.icon;
		}

		public void setIcon(String icon) {
			this.icon = icon;
		}

		private String id;

		public String getId() {
			return this.id;
		}

		public void setId(String id) {
			this.id = id;
		}

		private String name;

		public String getName() {
			return this.name;
		}

		public void setName(String name) {
			this.name = name;
		}

		private OpeningHours opening_hours;

		public OpeningHours getOpeningHours() {
			return this.opening_hours;
		}

		public void setOpeningHours(OpeningHours opening_hours) {
			this.opening_hours = opening_hours;
		}

		private ArrayList<Photo> photos;

		public ArrayList<Photo> getPhotos() {
			return this.photos;
		}

		public void setPhotos(ArrayList<Photo> photos) {
			this.photos = photos;
		}

		private String place_id;

		public String getPlaceId() {
			return this.place_id;
		}

		public void setPlaceId(String place_id) {
			this.place_id = place_id;
		}

		private double rating;

		public double getRating() {
			return this.rating;
		}

		public void setRating(double rating) {
			this.rating = rating;
		}

		private String reference;

		public String getReference() {
			return this.reference;
		}

		public void setReference(String reference) {
			this.reference = reference;
		}

		private String scope;

		public String getScope() {
			return this.scope;
		}

		public void setScope(String scope) {
			this.scope = scope;
		}

		private ArrayList<String> types;

		public ArrayList<String> getTypes() {
			return this.types;
		}

		public void setTypes(ArrayList<String> types) {
			this.types = types;
		}

		private String vicinity;

		public String getVicinity() {
			return this.vicinity;
		}

		public void setVicinity(String vicinity) {
			this.vicinity = vicinity;
		}

		@Override
		public String toString() {
			return "Result [geometry=" + geometry + ", icon=" + icon + ", id=" + id + ", name=" + name
					+ ", opening_hours=" + opening_hours + ", photos=" + photos + ", place_id=" + place_id + ", rating="
					+ rating + ", reference=" + reference + ", scope=" + scope + ", types=" + types + ", vicinity="
					+ vicinity + "]";
		}
		
	}

	public class NearJsonResult {
		private ArrayList<Object> html_attributions;

		public ArrayList<Object> getHtmlAttributions() {
			return this.html_attributions;
		}

		public void setHtmlAttributions(ArrayList<Object> html_attributions) {
			this.html_attributions = html_attributions;
		}

		private ArrayList<Result> results;

		public ArrayList<Result> getResults() {
			return this.results;
		}

		public void setResults(ArrayList<Result> results) {
			this.results = results;
		}

		private String status;

		public String getStatus() {
			return this.status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		@Override
		public String toString() {
			return "NearJsonResult [html_attributions=" + html_attributions + ", results=" + results + ", status="
					+ status + "]";
		}
		

	}
	
	
}