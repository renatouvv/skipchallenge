package com.skip.challenge.model;

public class Restaurant {
	
	private Address address;
	
	private String name;
	
	private String placeId;
	
	private Boolean open;
	
	private Double rating;
	
	private String vicinity;

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPlaceId() {
		return placeId;
	}

	public void setPlaceId(String placeId) {
		this.placeId = placeId;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	public Double getRating() {
		return rating;
	}

	public void setRating(Double rating) {
		this.rating = rating;
	}

	public String getVicinity() {
		return vicinity;
	}

	public void setVicinity(String vicinity) {
		this.vicinity = vicinity;
	}

	@Override
	public String toString() {
		return "Restaurants [address=" + address + ", name=" + name + ", placeId=" + placeId + ", open=" + open
				+ ", rating=" + rating + ", vicinity=" + vicinity + "]";
	}
	
}