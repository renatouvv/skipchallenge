package com.skip.challenge.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.skip.challenge.model.Address;
import com.skip.challenge.model.AddressJsonObject.ResultJsonObject;
import com.skip.challenge.model.NearRestaurantsJsonObject.NearJsonResult;
import com.skip.challenge.model.NearRestaurantsJsonObject.Result;
import com.skip.challenge.model.Restaurant;

@Service
public class SkipChallengeServiceImpl implements SkipChallengeService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SkipChallengeServiceImpl.class);
	
	private static String key = "TIzaSyDDqltEebXqvwfnKPKUFsC77ylDzhH4HCs";
	private static String locationSearchByAddressUrl = "https://maps.googleapis.com/maps/api/geocode/json";
	private static String placeSearchBaseUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json";
	
	protected String buildSearchUrl(Address address) {
		StringBuilder formedUrl = new StringBuilder().append(placeSearchBaseUrl);
		formedUrl.append("?location=").append(address.getLat()).append(",").append(address.getLon());
		formedUrl.append("&radius=500&type=restaurant");
		formedUrl.append("&key=").append(key);
		return formedUrl.toString();
	}
	
	protected String buildBaseAddress() {
		StringBuilder builder = new StringBuilder().append("Avenida+Lineu+de+Paula+Machado+,+1088+").append("-").append("+Cidade+Jardim+").append("-").append("+SP").append("&key=").append(key);
		return builder.toString();
	}
	
	public Address getBasePlaceForNearSearches() {
		
		String baseAddress = this.buildBaseAddress();
		StringBuilder builder = new StringBuilder().append(locationSearchByAddressUrl).append("?address=").append(baseAddress).append("&sensor=true");
		Address address = new Address();
		LOGGER.info("URL: " + builder.toString());
		try {
			HttpURLConnection conn = this.getConnection(builder.toString());
	        
	        if (conn.getResponseCode() != 200) {
	        	LOGGER.info("Failed: HTTP error code: " + conn.getResponseCode());
        		return address;
	        }
	        
	        ResultJsonObject gson = this.convertJsonToObject(conn);
	        		
	        address.setLat(gson.getResults().get(0).getGeometry().getLocation().getLat());
	        address.setLon(gson.getResults().get(0).getGeometry().getLocation().getLng());
	        
	        LOGGER.info("ADDRESS: " + address.toString());
	        
	        this.closeConnection(conn);
	        
		} catch (Exception e) {
			LOGGER.error("A problem ocurred when trying to retrieve the location by a address.", e);
		}
		return address;
	}
	
	public List<Restaurant> findNearPlaces(Address address) {
		
		String formedUrl = this.buildSearchUrl(address);
		LOGGER.info("FORMED URL: " + formedUrl);
		List<Restaurant> restaurants = Collections.emptyList();
		try {
			HttpURLConnection conn = this.getConnection(formedUrl);
			
	        if (conn.getResponseCode() != 200) {
	        	LOGGER.info("Failed: HTTP error code: " + conn.getResponseCode());
        		return restaurants;
	        }
	        
	        NearJsonResult gson = (NearJsonResult) this.convertNearJsonToObject(conn);
	        
	        restaurants = filterRestaurantDataToPresent(gson);
	        LOGGER.info("SEARCH RESULT: " + restaurants.size());
	        
	        this.closeConnection(conn);
	        
		} catch (Exception e) {
			LOGGER.error("A problem ocurred when trying to retrieve the search nearby address.", e);
		}
		
		return restaurants;
	}
	
	protected ResultJsonObject convertJsonToObject(HttpURLConnection conn) {
		ResultJsonObject gson = null;
		try {
			
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

	        String output = "", full = "";
	        while ((output = br.readLine()) != null) {
	            full += output;
	        }
	        
	        gson = new Gson().fromJson(full, ResultJsonObject.class);
			
		} catch (Exception e) {
			LOGGER.error("It was not possible to convert Json to Object.");
		}
		
        return gson;
	}
	
	protected NearJsonResult convertNearJsonToObject(HttpURLConnection conn) {
		NearJsonResult gson = null;
		try {
			
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

	        String output = "", full = "";
	        while ((output = br.readLine()) != null) {
	            full += output;
	        }
	        
	        gson = new Gson().fromJson(full, NearJsonResult.class);
			
		} catch (Exception e) {
			LOGGER.error("It was not possible to convert Json to Object.");
		}
		
        return gson;
	}
	
	protected HttpURLConnection getConnection(String formedUrl) {
		HttpURLConnection conn = null;
		try {
			URL url = new URL(formedUrl);
			conn = (HttpURLConnection) url.openConnection();
	        conn.setRequestMethod("GET");
	        conn.setRequestProperty("Accept", "application/json");
		} catch (Exception e) {
			LOGGER.error("It was not possible to open a connection.", e);
		}
		return conn;
	}
	
	protected void closeConnection(HttpURLConnection conn) {
		conn.disconnect();
	}
	
	protected List<Restaurant> filterRestaurantDataToPresent(NearJsonResult gson) {
		List<Restaurant> restaurants = new ArrayList<Restaurant>();
		if (gson != null) {
			for (Result result : gson.getResults()) {
				Restaurant restaurant = new Restaurant();
				Address address = new Address();
				address.setLat(result.getGeometry().getLocation().getLat());
				address.setLon(result.getGeometry().getLocation().getLng());
				restaurant.setAddress(address);
				restaurant.setName(result.getName());
				if (result.getOpeningHours() != null) {
					restaurant.setOpen(result.getOpeningHours().getOpenNow());
				} else {
					restaurant.setOpen(false);
				}
				restaurant.setPlaceId(result.getPlaceId());
				restaurant.setRating(result.getRating());
				restaurant.setVicinity(result.getVicinity());
				
				restaurants.add(restaurant);
			}
		}
		
		return restaurants;
	}

}
