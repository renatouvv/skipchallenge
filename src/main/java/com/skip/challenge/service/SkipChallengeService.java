package com.skip.challenge.service;

import java.util.List;

import com.skip.challenge.model.Address;
import com.skip.challenge.model.Restaurant;

public interface SkipChallengeService {
	
	public Address getBasePlaceForNearSearches();
	
	public List<Restaurant> findNearPlaces(Address address);

}